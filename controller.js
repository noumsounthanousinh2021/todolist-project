const fs = require("fs");
class appController {
    constructor(id, title, des,) {
        this.id = id
        this.title = title
        this.des = des
    }

    create = () => {
        try {
            fs.access('jsfile.json', (err) => {
                if (err) {
                    fs.writeFileSync('jsfile.json', JSON.stringify([]))
                }
                const todoBuffer = fs.readFileSync("jsfile.json");
                let dataJSON = todoBuffer.toString();
                const todos = JSON.parse(dataJSON);
                const duplicateTodo = todos.find((todo) => {
                    return todo.title === this.title;
                })

                if (!duplicateTodo) {
                    todos.push({
                        id: this.id,
                        title: this.title,
                        des: this.des,
                    });
                    dataJSON = JSON.stringify(todos);
                    fs.writeFileSync("jsfile.json", dataJSON);
                    console.log("New Todo Added");
                } else {
                    console.log("New Todo title has already been used");
                }
            })

        } catch (error) {
            console.log("An error occured, try again")
        }

    };

    update = () => {
        try {
            fs.access('jsfile.json', (err) => {
                if (err) {
                    fs.writeFileSync('jsfile.json', JSON.stringify([]))
                }
                const todoBuffer = fs.readFileSync("jsfile.json");
                let dataJSON = todoBuffer.toString();
                const todos = JSON.parse(dataJSON);
                const duplicateTodo = todos.find((todo) => {
                    return todo.id === this.id;
                })

                if (duplicateTodo) {
                    const remain = todos.filter((item) => {
                        return item.id != this.id;
                    })
                    let listData = JSON.stringify(duplicateTodo)
                    const oldData = JSON.parse(listData);

                    let isId = this.id
                    let isTitle = oldData.title;
                    let isdes = oldData.des;

                    if (this.title != null) {
                        isTitle = this.title
                    }
                    if (this.des != null) {
                        isdes = this.des
                    }
                    let updated = [{
                        id: isId.toString(),
                        title: isTitle,
                        des: isdes,
                    }]
                    let updatedFileData = [...remain, ...updated]
                    dataJSON = JSON.stringify(updatedFileData);
                    fs.writeFileSync("jsfile.json", dataJSON);
                    console.log('Updates successfully');
                } else {
                    console.log(`Not found this Id '${this.id}' `);
                }
            })


        } catch (error) {
            console.log("An error occured")
        }
    }

    delete = () => {
        try {
            const todoBuffer = fs.readFileSync("jsfile.json");
            let dataJSON = todoBuffer.toString();
            const todos = JSON.parse(dataJSON);

            const remain = todos.filter((item) => {
                return item.id != this.id;
            })

            dataJSON = JSON.stringify(remain);

            fs.writeFileSync("jsfile.json", dataJSON);

            if (remain.length === todos.length) {
                console.log("This file does not exist")
            } else {
                console.log("Todo was deleted succesfully")
            }
        } catch (error) {
            console.log("An error just occured")
        }
    }

    filter = (title) => {
        try {
            const todoBuffer = fs.readFileSync("jsfile.json");
            let dataJSON = todoBuffer.toString();
            const todos = JSON.parse(dataJSON);

            const Todo = todos.find((item) => {
                return item.title === title;
            })

            console.log(Todo);

        } catch (error) {
            console.log("An error occured")
        }
    }

    all = () => {
        try {
            const todoBuffer = fs.readFileSync("jsfile.json");
            let dataJSON = todoBuffer.toString();
            const todos = JSON.parse(dataJSON);

            console.log(todos)

        } catch (error) {
            console.log("An error occured")
        }
    }
}

module.exports = appController
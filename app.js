const yargs = require("yargs"); 
const appControllers = require('./controller')



yargs.command({
    command: "add",
    describe: "Add a new task",
    builder: {
        title: {
            describe: "task title",
            type: "string",
            demandOption: true,
        },
        des: {
            describe: "task description",
            type: "string",
            demandOption: true,
        },
    },
    handler: function (argv) {
        const rnd = Math.floor((Math.random() * 10000000) + 1).toString()
        const res = new appControllers(rnd, argv.title, argv.des).create()
    },
});

yargs.command({
    command: "list",
    describe: "Get all task",
    handler: function () {
        const res = new appControllers().all()
    },
});

yargs.command({
    command: "filter",
    describe: "get a specific task with the title",
    builder: {
        title: {
            describe: "task title",
            type: "string",
            demandOption: true,
        }
    },
    handler: function (argv) {
        const res = new appControllers().filter(argv.title)
    },
});

yargs.command({
    command: "update",
    describe: "update a specific task with the id",
    builder: {
        id: {
            describe: "task Id",
            type: "string",
            demandOption: true,
        },
        title: {
            describe: "task title",
            type: "string",
            demandOption: false,
        },
        des: {
            describe: "task description",
            type: "string",
            demandOption: false,
        }
    },
    handler: function (argv) {
        const res = new appControllers(argv.id, argv.title, argv.todo).update()
    },
});

yargs.command({
    command: "delete",
    describe: "Remove a specific task with the id",
    builder: {
        id: {
            describe: "task title",
            type: "string",
            demandOption: true,
        }
    },
    handler: function (argv) {
        const res = new appControllers(argv.id).delete()
    },
});


yargs.parse();